<?php
require_once 'lib/tpl.php';
$connect = new PDO("sqlite:contactsData.sqlite");
$connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$doit = arg('doit')?arg('doit'):"contacts";
$data = [];
$contact_data = [];
$current_year = date('Y');
$id = arg('person_id');
if($doit === 'save'){
    if(strlen(arg('firstName')) < 2 || strlen(arg('lastName')) < 2 || strlen(arg('lastName')) > 50 || strlen(arg('firstName')) > 50 ) {
        $data['$form_or_table'] = "form_template.html";
        $data['$message'] = 'Must be from 2 to 50 chars in Name and/or Surname';
        print render_template('template_main.html', $data);
    }
    else {
        $firstName = arg('firstName');
        $secondName = arg('lastName');
        $phone1 = arg('phone1');
        $phone2 = arg('phone2');
        $phone3 = arg('phone3');
        $input = $connect->prepare("INSERT INTO contacts(firstName, secondName) VALUES('$firstName', '$secondName')");
        $input->execute();
        $input = $connect->prepare("INSERT INTO phones (phone1, phone2, phone3) VALUES ('$phone1','$phone2','$phone3')");
        $input->execute();
        header('Location: index.php?doit=contacts&result=added');
    }
}
if($doit === 'contacts'){
    $input = $connect->prepare('SELECT * from contacts, phones WHERE id_con = id_ph');
    $input->execute();
    foreach ($input as $item){
        $united_phone = $item['phone1'].' '.$item['phone2'].' '.$item['phone3'];
        array_push($contact_data, new Person($item['firstName'], $item['secondName'], $united_phone, $item['id_con']));
    }
    if(arg('result') === "added"){
        $data['$message'] = "Added!";
    }
    if(arg('result')=== 'updated'){
        $data['$message'] = 'Updated';
    }
    $data['$form_or_table'] = 'table_template.html';
    $data['$contact_data'] = $contact_data;
    $data['$current_year'] = $current_year;
    print render_template('template_main.html', $data);
}
elseif ($doit === 'add'){
    $data['$form_or_table'] = 'form_template.html';
    $data['$current_year'] = $current_year;
    $data['$action'] = 'save';
    print render_template('template_main.html', $data);
}
elseif ($doit === 'edit') {
    $con = $connect->prepare("SELECT firstName, secondName from contacts WHERE id_con = :id;");
    $con -> bindParam(':id', $id);
    $con -> execute();
    $ph = $connect->prepare('SELECT phone1, phone2, phone3 FROM phones WHERE id_ph = :id;');
    $ph -> bindParam(':id', $id);
    $ph ->execute();
    $data['$id'] = $id;
    foreach ($con as $item) {
        $data['$name'] = $item['firstName'];
        $data['$sur'] = $item['secondName'];
    }
    foreach ($ph as $item) {
        $data['$telephone1'] = $item['phone1'];
        $data['$telephone2'] = $item['phone2'];
        $data['$telephone3'] = $item['phone3'];
    }
    $data['$form_or_table'] = "form_template.html";
    $data['$current_year'] = $current_year;   
    $data['$action'] = "edited";
    print render_template('template_main.html', $data);
}
elseif($doit === 'edited'){
    $data['$action'] = "edited";
    if(strlen(arg('firstName')) < 2 || strlen(arg('lastName')) < 2 || strlen(arg('lastName')) > 50 || strlen(arg('firstName')) > 50 ){
        $data['$form_or_table'] = 'form_template.html';
        $data['$name'] = arg('firstName');
        $data['$sur'] = arg('lastName');
        $data['$telephone1'] = arg('phone1');
        $data['$telephone2'] = arg('phone2');
        $data['$telephone3'] = arg('phone3');
        $data['$message'] = 'Must be from 2 to 50 chars in Name and/or Surname';
        $data['$current_year'] = $current_year;
    }
    else{
        $id_update = arg('id');
        $a = arg('firstName');
        $b = arg('lastName');
        $input = $connect->prepare("UPDATE contacts SET firstName= :f, secondName= :s WHERE id_con = :id;");
        $input -> bindParam(':id', $id_update);
        $input -> bindParam(':f', $a);
        $input ->bindParam(':s', $b);
        $input->execute();
        $c = arg('phone1');
        $d = arg('phone2');
        $e = arg('phone3');
        $input = $connect->prepare("UPDATE phones SET phone1= :p1, phone2= :p2, phone3= :p3 WHERE id_ph = :id");
        $input ->bindParam(':id', $id_update);
        $input ->bindParam('p1', $c);
        $input ->bindParam('p2', $d);
        $input ->bindParam('p3', $e);
        $input->execute();
        header('Location: index.php?doit=contacts&result=updated');
    }
    print render_template('template_main.html', $data);
}

function arg($key) {
    if (isset($_GET[$key])) {
        return $_GET[$key];
    } else if (isset($_POST[$key])) {
        return $_POST[$key];
    } else {
        return '';
    }
}
class Person{
    public $name;
    public $lastname;
    public $phones;
    public $id_con;

    public function __construct(
        $name, $lastname, $phones, $id_con) {
        $this->name = $name;
        $this->lastname = $lastname;
        $this->phones = $phones;
        $this->id_con = $id_con;
    }
}
